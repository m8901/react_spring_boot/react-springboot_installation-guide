# react+springboot_installation-guide

react+springboot 개발 환경 구축 가이드

## 설치 순서 (작성일:2021-10-18) ##

### 1. Git ###
>###### [1.1. Git 다운로드](https://git-scm.com/)  ######
>###### [1.2. Git 설치방법](https://goddaehee.tistory.com/216)  ######
>###### [1.3. Gitlab 가입 및 설정](https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=srader&logNo=221438056211)  ######

### 2. Node Js ###
>###### [2.1. Node Js 다운로드](https://nodejs.org/ko/download/) ######
>###### [2.2. Node Js 설치방법](https://urmaru.com/9) ######

### 3. VS CODE ###
>###### [3.1. VS CODE 다운로드](https://code.visualstudio.com/)  ######
>###### [3.2. VS CODE 설치방법](https://webnautes.tistory.com/1197)  ######

### 4. Spring Tools 4 ###
>###### [4.1. Spring Tools 4 다운로드](https://spring.io/tools)  ######
>###### [4.2. Spring Tools 4 설치방법](https://kimvampa.tistory.com/150)  ######

### 5. open jdk ###
>###### [5.1. open jdk 다운로드](https://github.com/ojdkbuild/ojdkbuild)  ######
>###### [5.2. open jdk 설치방법](https://www.jeu.kr/28)  ######

### 6. lombok ###
>##### [6.1. lombok 다운로드](https://projectlombok.org/download)  
>##### [6.2. lombok 설치방법](https://congsong.tistory.com/31)  

### 7. SSMS ###
>###### [7.1. SSMS 다운로드](https://docs.microsoft.com/ko-kr/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)  
>###### [7.2. SSMS 설치방법](https://www.098.co.kr/ssmssql-server-management-studio-%EC%84%A4%EC%B9%98/) 

### 8. Apache Tomcat ###
>###### [8.1. Apache Tomcat 다운로드](http://tomcat.apache.org/whichversion.html)  
>###### [8.2. Apache Tomcat 설치방법](https://tokoyy.tistory.com/7) 

### 9. sts4 설정 및 gitlab 연동 ###

### 10. 실행 및 예제 ###

### 11. Git Lab Commit ###
>###### 11.1.Synchronize Incoming  ######
>###### 11.2. Git Staging 추가  ######
>###### 11.3. Commit 진행 및 업로드 확인 ######
